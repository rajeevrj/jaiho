import React from 'react';

import { Modal, Header, Body, Footer, Button} from  'react-bootstrap';


//You have to use this component as follows
// <ModalDialog  showModal={true} close={this.close}/>
// showModal: true/false bollean value
// close: is a callback function which is written in parent class
//similarly like close() we can write a callback function for "saveChange" 

const ModalDialog = (props) =>{
    return(
        <Modal show={props.showModal} onHide={()=>props.close(props.showModal)}>

            <Modal.Header>
                <Modal.Title>Modal title</Modal.Title>
            </Modal.Header>

            <Modal.Body>
                One fine body...
            </Modal.Body>

            <Modal.Footer>
                <Button onClick={()=>props.close(props.showModal)}>Close</Button>
                <Button bsStyle="primary">Save changes</Button>
            </Modal.Footer>

        </Modal>
    )
        
}


export default ModalDialog;
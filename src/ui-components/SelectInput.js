import React from 'react';


/*
  :::Implementation as below:::
  ---------------------------------
          <SelectInput onSelectChange={this.handleSelectChange.bind(this)} 
               label="username"  
               position="left" 
               required="true"   
               labelStyle={{ fontWeight: 'bold'}} 
               inputStyle={{fontWeight:'bold'}}
               selectArr = { [{ value: 'value 1'}, { value: 'value 2'}, { value: 'value 3'}] }     
          />
  all props are optional, you can pass if it is needed.
  required: TRUE (if we want to put aestrick) 
  we can pass inline style using "labelStyle" and "inputStyle"
  
  >> From parent page pass "onSelectChange={this.handleSelectChange.bind(this)}" for callback
     `````````````````````````````````````````````````````
    handleSelectChange(e){
        console.log('radio change ', e.target.value)
    }  
     `````````````````````````````````````````````````````
 */
class SelectInput extends React.Component { 
  
  constructor (props) {
    super(props);
  }

  render() {
    const {position, label, required,  labelStyle,inputStyle, selectArr}= this.props; 
    
    return(
         <div className="textinput-wrapper">
            <div>
                <span className={"input-label "+ (required==='true'?'required':'')+ " "+(label ? '': 'hide')} 
                      style={labelStyle}>{label}
                </span>
                <select onChange={(e)=>this.props.onSelectChange(e)} 
                       style={inputStyle} 
                       className={"inline-text-input "+(position === 'top' ? 'block':'')}
                >
                {
                    selectArr.map((item, i)=>  <option key={i} value={item.value}>{item.value}</option>)
                }
                </select>
            </div>
         </div>
    )
  }
}

export default SelectInput;
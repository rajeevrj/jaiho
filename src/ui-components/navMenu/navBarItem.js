import React from 'react';
import PropTypes from 'prop-types';
import NavBarLink from './navBarLink';
import NavBar from './navBar';
//import Container from '../container/Container';

export class NavBarItem extends React.Component {
  
  
  generateLink(){
    //Right now we don't need our class but what if we wanted to change the text, add an arrow or something? 
    //Single responsibility principles tell us that it our "Item" should not handle this.
    return <NavBarLink url={this.props.url} text={this.props.text} />;
  }

  generateSubmenu(){
    //We generate a simple Navbar (the parent) 
    //Spoilers: it takes items as its argument
    return <NavBar items={this.props.submenu} />
    
  }

  generateContent(){
    var content = [this.generateLink()];
    if(this.props.submenu){
      //If there is a submenu in our data for this item
      //We add a generated Submenu to our content
      content.push(this.generateSubmenu());
    }
    return content;
  }

  render() {
    let Component = require("../../page-components/message_center/index").default;
    console.log(this.props.type);
    if(this.props.type && this.props.type ==="component"){
      
      
      return (
        <li> 
          <div  className="dynamic-table-align-right">
              <Component />
          </div>
        </li>
      );
    }else{
      var content = this.generateContent();
      return (
        <li>
          {content}
        </li>
      );
    }
      
    
    
    
  }
};

export default NavBarItem;


 // if(this.props.submenu.type && this.props.submenu ==="component"){
 //      // let comp1 = require("../../widgets/message_center/index").default;
 //      //       return <ul className="menu">
 //      //         <li>
 //      //           <comp1 />
 //      //         </li>
 //      //       </ul>

 //            var content = this.generateContent();
 //      return (
 //        <li>
 //          {content}
 //        </li>
 //      );
      

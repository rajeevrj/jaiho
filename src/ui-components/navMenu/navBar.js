import React from 'react';
import PropTypes from 'prop-types';
import NavBarItem from './navBarItem';

export class NavBar extends React.Component {
  


  generateItem(item){
    return <NavBarItem text={item.text} url={item.url} submenu={item.submenu} type={item.type}/>
  }

  render() {
    var items = this.props.items.map(this.generateItem);
    return (
      <ul className="menu">
      {items}
      </ul>
    );
  }
};

export default NavBar;




export default {data:[
  {
    "text": "Link 1",
    "url": "#",
    "type":"static"
  },
  {
    "text": "Link 2",
    "url": "#",
    "type":"static"
  },
  {
    "text": "Link 3",
    "url": "#",
    "type":"static",
    "submenu": [
      {
        "text": "Sublink 1",
        "url": "#",
        "type":"static",
        "submenu": [
          {
            "text": "SubSublink 1",
            "url": "#",
            "type":"static"
          }
        ]
      },
      {
        "text": "Sublink 2",
        "url":"#",
        "type":"static",
        "submenu": [{"text":"MessageCenter", "url":"#", "type":"component"}]
      }
    ]
  }
]}



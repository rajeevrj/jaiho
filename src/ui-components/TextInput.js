import React from 'react';


/*
  :::Implementation as below:::
  ---------------------------------
  <TextInput onInputChange={this.handleChange.bind(this)} label="username"  position="top" required="true" placeholder="placeholder..."  labelStyle={{color: 'red', fontWeight: 'bold'}} inputStyle={{fontWeight:'bold'}}/>
  all props are optional, you can pass if it is needed.
  required: TRUE (if we want to put aestrick) 
  we can pass inline style using "labelStyle" and "inputStyle"
  
  >> From parent page pass "onInputChange={this.handleChange.bind(this)}" for callback
     `````````````````````````````````````````````````````
     handleChange(e){
         //this code will be at parent paage
        console.log('handleChange called ', e.target.value)
     }
     `````````````````````````````````````````````````````
 */
class TextInput extends React.Component {
  
  constructor (props) {
    super(props);
  }

  render() {
    const {position, label, required, placeholder, labelStyle, inputStyle}= this.props;  
    return(
         <div className="textinput-wrapper">
            <div>
                <span className={"input-label "+ (required==='true'?'required':'')} style={labelStyle}>{label}</span>
                <input onChange={(e)=>this.props.onInputChange(e)} type="text" placeholder={placeholder} style={inputStyle} className={"inline-text-input "+(position === 'top' ? 'block':'')}/>
            </div>
         </div>
    )
  }
}

export default TextInput;
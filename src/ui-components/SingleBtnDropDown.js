import React from 'react';

import { ButtonToolbar, DropdownButton, MenuItem} from  'react-bootstrap';

//pass props as >> menuItem = [{title: 'menu name1'}, {title: 'menu name2'}]
const SingleBtnDropDown = (props) =>{
    return(
        <ButtonToolbar>
            <DropdownButton bsStyle="default" title="No caret" noCaret id="dropdown-size-medium">
                {
                    props.menuItem.map((item, i)=>{
                        return (<MenuItem eventKey={i}>Action</MenuItem>);
                    })
                }
            </DropdownButton>
        </ButtonToolbar>
    )
        
}


export default SingleBtnDropDown;
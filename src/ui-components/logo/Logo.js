import React from 'react';
import {IMG_BASE_URL} from '../../constants.js';

const Logo = (props) => <img src={IMG_BASE_URL+props.src} style={props.style} />

export default Logo; 
import React from 'react';

/*
  :::Implementation as below:::
  ---------------------------------
  <CheckboxInput required="true"
                 title="which coutry is from EUROPE ?" 
                 checkBoxAlignment ="horizontal"  
                 chkboxGroupArr={[{label: 'INDIA', name: 'country'},{label: 'UK', name: 'country'},{label: 'POLAND', name: 'country'}]}
  />
  
  all props are optional, EXCEPT "chkboxGroupArr"
  radioAlignment: "horizontal" OR "vertical"
  required: TRUE (if we want to put aestrick) 
  we can pass "titleStyle" for styling title using inline css
  
  >> From parent page pass "handleCheckboxChange = {this.handleCheckboxChange.bind(this)}" for callback
     `````````````````````````````````````````````````````
     handleCheckboxChange(e){
         //this code will be at parent paage
        console.log('checkbox clicked ', e.target.value)
     }
     `````````````````````````````````````````````````````
 */
class CheckboxInput extends React.Component {
  
  constructor (props) {
    super(props);
    this.renderVertical = this.renderVertical.bind(this);
    this.renderHorizontal = this.renderHorizontal.bind(this);    
  }

  renderVertical(item, i){
      return(
          <div className="radio-element-vertical" key={i}>
                <input onChange={(e)=>this.props.handleCheckboxChange(e)} type="checkbox" name={item.name} value={item.value} /> &nbsp; 
                <div className="radio-element-label">{item.value}</div>
          </div>
      )
  }

   renderHorizontal(item, i){
      return(
          <div className="radio-element-horizontal" key={i}>
                <input onChange={(e)=>this.props.handleCheckboxChange(e)} type="checkbox" name={item.name} value={item.value} /> &nbsp; 
                <span className="radio-element-label">{item.value}</span>
          </div>
      )
  }
  
  

  render() {
    const {title, chkboxGroupArr, checkBoxAlignment, required, titleStyle}= this.props;  
    return(
         <div className="radioinput-wrapper">
            <div>
                <p className={"input-label "+ (required==='true'?'required':'')} style={titleStyle}>{title}</p>
                {
                    chkboxGroupArr.map((item, i)=>{
                        return checkBoxAlignment === 'horizontal' ? this.renderHorizontal(item, i)  : this.renderVertical(item, i) 
                    })
                }
            </div>
         </div>
    )
  }
}

export default CheckboxInput;
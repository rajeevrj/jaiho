import React from 'react';

//it accepts iconPlacement
//iconPlacement can be left || right only
const TextIcon = (props) => {
    if(props.iconPlacement=='left'){
       return (
      	  <a href="">
             <i className={props.icon} />&nbsp;
             {props.title}
          </a>       
       )
    }
    else if(props.iconPlacement=='right'){
       return (
      	  <a href="">
             {props.title}&nbsp;
             <i className={props.icon} />
          </a>       
       )
    }
    else{
        return(<span>wrong placement</span>)
    }
}

export default TextIcon;

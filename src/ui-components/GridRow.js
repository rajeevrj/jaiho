import React from 'react';

export class GridRow extends React.Component {
  
  render() {    
    let classname = this.props.className;
    return (
      <div className={ "row "+ (classname ? classname : '' )}>{this.props.children}</div>
    );    
  }

}  


export default GridRow;


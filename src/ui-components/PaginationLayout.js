import React from 'react';
import { Pagination } from  'react-bootstrap';


// this component can be used as below
//<PaginationLayout maxbtns={3} totalitems={15}/>
// onSelect={this.handleSelect} can be rewritten to use as callback function if we pass a callback fn from parent

const PaginationLayout = React.createClass({
  getInitialState() {
    return {
      activePage: 1
    };
  },

  handleSelect(eventKey) {
    this.setState({
      activePage: eventKey
    });
  },

  render() {
    return (
      <Pagination
        prev
        next
        first
        last
        ellipsis
        boundaryLinks
        items={this.props.totalitems}
        maxButtons={this.props.maxbtns}
        activePage={this.state.activePage}
        onSelect={this.handleSelect} />
    );
  }
});

export default PaginationLayout;
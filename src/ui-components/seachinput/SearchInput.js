import React from 'react';

const SearchInput = (props) => {
    return(
        <span>
            <input type="text" className="inline-text-input" />
            <button className="btn btn-sm btn-primary searchinput-btn no-border"><i className="fa fa-search"/></button>
        </span>
        
    )
}

export default SearchInput; 

import React from 'react';
import PropTypes from 'prop-types';

const Label = (
    {
        className, 
        fontSize, 
        fontStyle, 
        color, 
        fontWeight,
        fontFamily,
        display,
        title
    }) =>(
  <div className={ className } style={{fontSize, fontStyle, color, fontWeight, fontFamily, display}}>
    {title}
  </div>
);

Label.propTypes = {
  className: PropTypes.string,
  fontSize: PropTypes.string,
  fontStyle: PropTypes.string,
  color: PropTypes.string, 
  fontWeight: PropTypes.string,
  fontFamily: PropTypes.string,
  title: PropTypes.string,
  display: PropTypes.string
};

Label.defaultProps = {
  // Add default props here.
  className: 'test class'
};


export default Label;


import React from 'react';
import PropTypes from 'prop-types';

export class Grid extends React.Component {
  
  

  render() {    
    let {className} = this.props;    
    return (
      <div className={ className }>{this.props.children}</div>
    );    
  }

}  


export default Grid;


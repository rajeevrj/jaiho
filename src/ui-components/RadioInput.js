import React from 'react';

/*
  :::Implementation as below:::
  ---------------------------------
  <RadioInput required="true"
              title="which coutry is from EUROPE ?" 
              radioAlignment="horizontal"  
              radioGroupArr={[{label: 'INDIA', name: 'country'},{label: 'UK', name: 'country'},{label: 'POLAND', name: 'country'}]}
  />
  
  all props are optional, EXCEPT "radioGroupArr"
  radioAlignment: "horizontal" OR "vertical"
  required: TRUE (if we want to put aestrick) 
  we can pass "titleStyle" for styling title using inline css
  
  >> From parent page pass "onRadioChange={this.onRadioChange.bind(this)}" for callback
     `````````````````````````````````````````````````````
     onRadioChange(e){
         //this code will be at parent paage
        console.log('radio change ', e.target.value)
     }
     `````````````````````````````````````````````````````
 */
class RadioInput extends React.Component {
  
  constructor (props) {
    super(props);
    this.renderVertical = this.renderVertical.bind(this);
    this.renderHorizontal = this.renderHorizontal.bind(this);    
  }

  renderVertical(item, i){
      return(
          <div className="radio-element-vertical" key={i}>
                <input onChange={(e) =>this.props.onRadioChange(e)} type="radio" name={item.name} value={item.label} /> &nbsp; 
                <div className="radio-element-label">{item.label}</div>
          </div>
      )
  }

   renderHorizontal(item, i){
      return(
          <div className="radio-element-horizontal" key={i}>
                <input type="radio"  onChange={(e) =>this.props.onRadioChange(e)} name={item.name} value={item.label} /> &nbsp; 
                <span className="radio-element-label">{item.label}</span>
          </div>
      )
  }
  
  

  render() {
    const {title, radioGroupArr, radioAlignment, required, titleStyle}= this.props;  
    return(
         <div className="radioinput-wrapper">
            <div>
                <p className={"input-label "+ (required==='true'?'required':'')} style={titleStyle}>{title}</p>
                {
                    radioGroupArr.map((item, i)=>{
                        return radioAlignment === 'horizontal' ? this.renderHorizontal(item, i)  : this.renderVertical(item, i) 
                    })
                }
            </div>
         </div>
    )
  }
}

export default RadioInput;
import React from 'react';

export class GridCol extends React.Component {
  
  render() {    
    return (
      <div className={ this.props.className }>{this.props.children}</div>
    );    
  }

}  


export default GridCol;


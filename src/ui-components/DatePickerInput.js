import React from 'react';
import DatePicker from 'react-datepicker';
import moment from 'moment';

import '../assets/css/react-datepicker.min.css';
import '../assets/css/react-datepicker-cssmodules.min.css';


class DatePickerInput extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      startDate: moment()
    };
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(date) {
    this.setState({
      startDate: date
    });
  }

  render() {
    return <div style={{display: 'inline-block'}}>
             <DatePicker
                dateFormat={this.props.dateFormat}
                selected={this.state.startDate}
                onChange={this.handleChange}
             />
           </div>;
  }
}

export default DatePickerInput;
import React from 'react';

const Dropdown = (props) => {
   return(
     <div>
        <a href="" id="dropdown-activator" className="dropdown-activator">&nbsp;
            Welcome, {props.user}  &nbsp; <i className="fa fa-chevron-down" />
        </a>
        <ul className="dropDown" style={{width: props.width+'px', minWidth: '200px'}}>
        {
            props.dropdown.map( (item, i)=> {
                 return (<li key={i}><a>{item.title}</a></li>)       
            })
        }
      </ul>
     </div>
      
  )
}
  

export default Dropdown;
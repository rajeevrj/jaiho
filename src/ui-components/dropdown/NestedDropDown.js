import React from 'react';

const NestedDropDown = (props) => {
   return(
      <div className="nestedDropDown" style={props.style}>
        <span className="nestedDropDown_title">DropDown text</span>
        <ul className="nestedDropDown-level1">
            {
                props.menuList.map( (item, i) =>{
                    return <li>{item.name}</li>
                })
            }
        </ul>
      </div>
  )
}
  

export default NestedDropDown;
import React from 'react';


import SGCaseForm from './SGCaseForm';

class SGCaseFormContainer extends React.Component {
   render() {
      return (
      	<div className="sg-case-contaier container">
            <div>
                <h3>Create SG Case</h3>
                <p>Enter the required information to create a new case in the system. It is strongly recommended that you 
                validate the case does not  already exist by using the case search.</p>
            </div>

            <SGCaseForm />
            
        </div>      
      );
   }
}


export default SGCaseFormContainer;

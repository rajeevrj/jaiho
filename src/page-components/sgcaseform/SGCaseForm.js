import React from 'react';
import FormHandlerButtons from './FormHandlerButtons';
import '../../assets/css/sgcaseform.scss';
import DatePickerInput from '../../ui-components/DatePickerInput';
import SelectInput from '../../ui-components/SelectInput';



class SGCaseForm extends React.Component {
    handleSelectChange(e){
        console.log(e.target.value);
    }    
    render(){
            return(
                <div>
                    
                    <FormHandlerButtons />

                    <div className="top-margin20">
                        <h4 className="txt-lightblue">Case information</h4>
                        <p><code>*</code> Required</p>
                    </div>

                    <div className="top-margin20 sg-form-box">
                        <h4><strong>Demographics</strong></h4>
                        
                        <div className="row">
                            <div className="col-md-5">
                                <div className="row ">
                                    <div className=" col-md-6 text-right">
                                        <span className="required">Case name: </span>
                                    </div>
                                    <div className="col-md-6">    
                                        <input type="text" className="inline-text-input"/>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-6">
                                <div className="row ">
                                    <div className=" col-md-6 text-right">
                                        <span >Industry code: </span>
                                    </div>
                                    <div className="col-md-6">    
                                        <input type="text" className="inline-text-input"/> <i className="fa fa-info-circle" />
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-md-5">
                                <div className="row ">
                                    <div className=" col-md-6 text-right">
                                        <span >Doing business as: </span>
                                    </div>
                                    <div className="col-md-6">    
                                        <input type="text" className="inline-text-input"/>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-6">
                                <div className="row ">
                                    <div className=" col-md-6 text-right">
                                        <span >Industry description: </span>
                                    </div>
                                    <div className="col-md-6">    
                                        
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-md-5">
                                <div className="row ">
                                    <div className=" col-md-6 text-right">
                                        <span >Case ID: </span>
                                    </div>
                                    <div className="col-md-6">    
                                        <input type="text" className="inline-text-input"/>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-6">
                                <div className="row ">
                                    <div className=" col-md-6 text-right">
                                        Original Effective date:
                                    </div>
                                    <div className="col-md-6"> 
                                        <DatePickerInput dateFormat="MM/DD/YYYY"/>  <i className="fa fa-calendar"/>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-md-5">
                                <div className="row ">
                                    <div className=" col-md-6 text-right">
                                        <span >Address 1: </span>
                                    </div>
                                    <div className="col-md-6">    
                                        <input type="text" className="inline-text-input"/>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-6">
                                <div className="row ">
                                    <div className=" col-md-6 text-right">
                                        <span >Anniversary date: </span>
                                    </div>
                                    <div className="col-md-6">    
                                        <input type="text"  placeholder="mm/dd" className="inline-text-input"/> 
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-md-5">
                                <div className="row ">
                                    <div className=" col-md-6 text-right">
                                        <span >Address 2: </span>
                                    </div>
                                    <div className="col-md-6">    
                                        <input type="text" className="inline-text-input"/>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-6">
                                <div className="row ">
                                    <div className=" col-md-6 text-right">
                                        <span >Renewal recipient delivery notice: </span>
                                    </div>
                                    <div className="col-md-6">    
                                        <SelectInput onSelectChange={this.handleSelectChange.bind(this)} 
                                            selectArr = { [{ value: 'value 1'}, { value: 'value 2'}, { value: 'value 3'}] }     
                                        />
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-md-5">
                                <div className="row ">
                                    <div className=" col-md-6 text-right">
                                        <span className="required">City: </span>
                                    </div>
                                    <div className="col-md-6">    
                                        <input type="text" className="inline-text-input"/>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-6">
                                <div className="row ">
                                    
                                </div>
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-md-5">
                                <div className="row ">
                                    <div className=" col-md-6 text-right">
                                        <span className="required">State: </span>
                                    </div>
                                    <div className="col-md-6">    
                                        <SelectInput onSelectChange={this.handleSelectChange.bind(this)} 
                                            selectArr = { [{ value: 'value 1'}, { value: 'value 2'}, { value: 'value 3'}] }     
                                        />
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-6">
                                <div className="row ">
                                    <span className="required">ZIP-code</span>
                                    <input type="text" className="inline-text-input" style={{width: '50px'}}/> 
                                    &#x268A;
                                    <input type="text" className="inline-text-input"  style={{width: '60px'}}/>                          
                                </div>
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-md-5">
                                <div className="row ">
                                    <div className=" col-md-6 text-right">
                                        <span >Country: </span>
                                    </div>
                                    <div className="col-md-6">    
                                        <SelectInput onSelectChange={this.handleSelectChange.bind(this)} 
                                            selectArr = { [{ value: 'value 1'}, { value: 'value 2'}, { value: 'value 3'}] }     
                                        />
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-md-5">
                                <div className="row ">
                                    <div className=" col-md-6 text-right">
                                        <span >Current carrier: </span>
                                    </div>
                                    <div className="col-md-6">    
                                        <SelectInput onSelectChange={this.handleSelectChange.bind(this)} 
                                            selectArr = { [{ value: 'value 1'}, { value: 'value 2'}, { value: 'value 3'}] }     
                                        />
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>





                    <div className="top-margin20 sg-form-box">
                        <h4><strong>Primary contact information</strong></h4>
                        
                        <div className="row">
                            <div className="col-md-5">
                                <div className="row ">
                                    <div className=" col-md-6 text-right">
                                        <span>Prefix: </span>
                                    </div>
                                    <div className="col-md-6">    
                                        <SelectInput onSelectChange={this.handleSelectChange.bind(this)} 
                                            selectArr = { [{ value: 'value 1'}, { value: 'value 2'}, { value: 'value 3'}] }     
                                        />
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-md-5">
                                <div className="row ">
                                    <div className=" col-md-6 text-right">
                                        <span >First name: </span>
                                    </div>
                                    <div className="col-md-6">    
                                        <SelectInput onSelectChange={this.handleSelectChange.bind(this)} 
                                            selectArr = { [{ value: 'value 1'}, { value: 'value 2'}, { value: 'value 3'}] }     
                                        />
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-6">
                                <div className="row ">
                                    <span>MI: </span>
                                    <input type="text" className="inline-text-input" style={{width: '50px'}}/> 
                                    &nbsp;
                                    <span>Suffix: </span>
                                    <input type="text" className="inline-text-input"  style={{width: '60px'}}/>                          
                                </div>
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-md-5">
                                <div className="row ">
                                    <div className=" col-md-6 text-right">
                                        <span>Last name: </span>
                                    </div>
                                    <div className="col-md-6">    
                                        <input type="text" className="inline-text-input" /> 
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-md-5">
                                <div className="row ">
                                    <div className=" col-md-6 text-right">
                                        <span>Title: </span>
                                    </div>
                                    <div className="col-md-6">    
                                        <input type="text" className="inline-text-input" /> 
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-md-5">
                                <div className="row ">
                                    <div className=" col-md-6 text-right">
                                        <span><input type="checkbox" /> </span>
                                    </div>
                                    <div className="col-md-6 text-left">    
                                        same address as above?
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-md-5">
                                <div className="row ">
                                    <div className=" col-md-6 text-right">
                                        <span>Address 1: </span>
                                    </div>
                                    <div className="col-md-6 text-left">    
                                        <input type="text" className="inline-text-input" />                                 
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-7">
                                <div className="row text-right">
                                    <div className=" col-md-3 text-right">
                                        <span>Phone number(s): </span>
                                    </div>
                                    <div className="col-md-9 text-left">    
                                        <SelectInput onSelectChange={this.handleSelectChange.bind(this)} 
                                            selectArr = { [{ value: 'value 1'}, { value: 'value 2'}, { value: 'value 3'}] }     
                                        />
                                        &nbsp;
                                        <input type="text" className="inline-text-input" placeholder="(###) ###-####" style={{width: '120px'}}/>      
                                        &nbsp;
                                        <span>EXT: </span>
                                        <input type="text" className="inline-text-input" style={{width: '100px'}}/>                                      
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div className="row">
                            <div className="col-md-5">
                                <div className="row ">
                                    <div className=" col-md-6 text-right">
                                        <span>Address 2: </span>
                                    </div>
                                    <div className="col-md-6 text-left">    
                                        <input type="text" className="inline-text-input" />                                 
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-7">
                                <div className="row text-right">
                                    <div className=" col-md-3 text-right">
                                        
                                    </div>
                                    <div className="col-md-9 text-left">    
                                        <SelectInput onSelectChange={this.handleSelectChange.bind(this)} 
                                            selectArr = { [{ value: 'value 1'}, { value: 'value 2'}, { value: 'value 3'}] }     
                                        />
                                        &nbsp;
                                        <input type="text" className="inline-text-input"  placeholder="(###) ###-####" style={{width: '120px'}}/>      
                                        &nbsp;
                                        <span>EXT: </span>
                                        <input type="text" className="inline-text-input" style={{width: '100px'}}/>                                      
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div className="row">
                            <div className="col-md-5">
                                <div className="row ">
                                    <div className=" col-md-6 text-right">
                                        <span>City: </span>
                                    </div>
                                    <div className="col-md-6 text-left">    
                                        <input type="text" className="inline-text-input" />                                 
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-7">
                                <div className="row text-right">
                                    <div className=" col-md-3 text-right">
                                        
                                    </div>
                                    <div className="col-md-9 text-left">    
                                        <SelectInput onSelectChange={this.handleSelectChange.bind(this)} 
                                            selectArr = { [{ value: 'value 1'}, { value: 'value 2'}, { value: 'value 3'}] }     
                                        />
                                        &nbsp;
                                        <input type="text" className="inline-text-input"  placeholder="(###) ###-####" style={{width: '120px'}}/>      
                                        &nbsp;
                                        <span>EXT: </span>
                                        <input type="text" className="inline-text-input" style={{width: '100px'}}/>                                      
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div className="row">
                            <div className="col-md-5">
                                <div className="row ">
                                    
                                </div>
                            </div>
                            <div className="col-md-7">
                                <div className="row text-right">
                                    <div className=" col-md-3 text-right">
                                        
                                    </div>
                                    <div className="col-md-9 text-left">    
                                        <SelectInput onSelectChange={this.handleSelectChange.bind(this)} 
                                            selectArr = { [{ value: 'value 1'}, { value: 'value 2'}, { value: 'value 3'}] }     
                                        />
                                        &nbsp;
                                        <input type="text" className="inline-text-input"  placeholder="(###) ###-####" style={{width: '120px'}}/>      
                                        &nbsp;
                                        <span>EXT: </span>
                                        <input type="text" className="inline-text-input" style={{width: '100px'}}/>                                      
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div className="row">
                            <div className="col-md-5">
                                <div className="row ">
                                    <div className=" col-md-6 text-right">
                                        <span >state: </span>
                                    </div>
                                    <div className="col-md-6">    
                                        <SelectInput onSelectChange={this.handleSelectChange.bind(this)} 
                                            selectArr = { [{ value: 'value 1'}, { value: 'value 2'}, { value: 'value 3'}] }     
                                        />
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-6">
                                <div className="row ">
                                    <span>ZIP code: </span>
                                    <input type="text" className="inline-text-input" style={{width: '50px'}}/> 
                                    &#x268A;
                                    <input type="text" className="inline-text-input"  style={{width: '60px'}}/>                          
                                </div>
                            </div>
                        </div>


                        <div className="row">
                            <div className="col-md-5">
                                <div className="row ">
                                    <div className=" col-md-6 text-right">
                                        <span>Email: </span>
                                    </div>
                                    <div className="col-md-6 text-left">    
                                        <input type="text" className="inline-text-input" />                                 
                                    </div>
                                </div>
                            </div>
                        </div>
            
                    </div>




                    <div className="top-margin20">
                        <h4><strong>Owner Assignment</strong></h4>
                        
                        <div className="row">
                            <div className="col-md-5">
                                <div className="row ">
                                    <div className=" col-md-6 text-right">
                                        <span>Product name: </span>
                                    </div>
                                    <div className="col-md-6">    
                                        
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-7 ">
                                <button className="btn btn-default btn-sm"><i className="fa fa-search" />&nbsp; search</button> 
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-md-5">
                                <div className="row ">
                                    <div className=" col-md-6 text-right">
                                        <span>Agency: </span>
                                    </div>
                                    <div className="col-md-6">    
                                        
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-md-5">
                                <div className="row ">
                                    <div className=" col-md-6 text-right">
                                        <span>managing agency: </span>
                                    </div>
                                    <div className="col-md-6 text-center">    
                                        
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-md-5">
                                <div className="row ">
                                    <div className=" col-md-6 text-right">
                                        <span>Sales rep: </span>
                                    </div>
                                    <div className="col-md-6">    
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                        

                    </div>


                <FormHandlerButtons />
                </div>
            )    
    }
    
}

export default SGCaseForm;
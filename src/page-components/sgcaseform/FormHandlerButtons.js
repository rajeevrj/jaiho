import React from 'react';

const FormHandlerButtons = () => {
    return(
        <div className="sg-formHandler-btns clearfix">
            <button className="btn btn-default btn-sm pull-left">Cancel</button>
            <button className="btn btn-primary btn-sm pull-right"><i className="fa fa-save" /> &nbsp; Save</button>
        </div>
    )
}

export default FormHandlerButtons;
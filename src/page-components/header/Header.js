import React from 'react';
import {textIconMenu, dropdown} from '../../config/uiconfig';
import Dropdown from '../../ui-components/dropdown/DropDown';
import TextIcon from '../../ui-components/texticon/TextIcon';
import Logo from '../../ui-components/logo/Logo';
import SearchInput from '../../ui-components/seachinput/SearchInput';
import SelectInput from '../../ui-components/SelectInput';
import NavBar from '../../ui-components/navMenu/navBar';
import NavData from '../../ui-components/navMenu/data';
import GridRow from '../../ui-components/GridRow';
import GridCol from '../../ui-components/GridCol';

class Header extends React.Component {
   
   constructor(){
       super();
       this.state = {ddwidth: 200}
   } 

   componentDidMount(){
       var ddwidth = document.getElementById('dropdown-activator').offsetWidth;
       this.setState({ddwidth});
   } 

   close(){
       console.log('close called');
   }

   handleSelectChange(e){
       console.log(e.target.value);
   }
   
   render() {
      
      return (
      	<div className="header-wrapper">

            <div className="header-menu-wrapper">    
                    <div className="container">
                        <div className="pull-right">
                            <ul className="ul-nav">
                            {
                                    textIconMenu.map( (item, i)=> {
                                        return(
                                            <li key={i}>
                                                <TextIcon {...item} iconPlacement="left"/>
                                            </li>       
                                        ) 
                                    }) 
                            }
                            <li>                              
                                <Dropdown dropdown={dropdown} user="Martin Umanager" width={this.state.ddwidth}/>
                            </li>
                            </ul>
                            
                        </div>
                    </div>
            </div> 
            
            <div className="container logo-searchinput-wrapper">
                <div  className="">
                    <div className="col-md-3">
                        <Logo src="logo.jpg" />
                        
                    </div>
                    <div className="col-md-9 text-right">
                        <SelectInput onSelectChange={this.handleSelectChange.bind(this)} 
                            selectArr = { [{ value: 'value 1'}, { value: 'value 2'}, { value: 'value 3'}] }     
                        /> &nbsp;
                        <SearchInput /><br />
                        <a href="">Advance Search</a>
                    </div>
                </div>      
            </div>

            <GridRow className="dynamic-menubar-wrapper">
              <GridCol className="container">
                    <NavBar items={NavData.data}/>
              </GridCol>
            </GridRow>
            
        </div>         
      );
   }
}


export default Header;


/*
    <img src='./src/assets/images/logo.jpg' alt="some error"/>
            Header  .
            <FontAwesome
        className='super-crazy-colors'
        name='rocket'
        size='2x'
        spin
        style={{ textShadow: '0 1px 0 rgba(0, 0, 0, 0.1)' }}
      />
           <i className="fa fa-calendar fa-5x"></i>
           <i className="glyphicon glyphicon-calendar fa-5x"></i>
        </div> 
*/
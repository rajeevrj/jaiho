import React from 'react';
import PropTypes from 'prop-types';
import Grid from '../../ui-components/grid/Grid';
import Label from '../../ui-components/label/Label';

export class MessageCenter extends React.Component {
  
  
  

  handleClick(e, url){
      //e.preventDefault();
      console.log('handle anchor click event', event, url);
  }

  render() {
    console.log("I am called");
    let {className, fontSize, fontWeight, fontFamily, color, url, title} = this.props;
    
    return (
      <Grid className="nopad border top20">
        <Grid className="nopad border">
            <Grid className="table-hdeader-black pad5"><Label  className="left-align" fontFamily="UHC Sans" fontWeight="bold" fontSize="14px" color="#fff" title="Messsage Center"></Label></Grid>
            <Grid>
              <Grid className="pad5 col-md-6 pull-right">
                <input type="text" placeholder="search" className="form-control pull-right" />
              </Grid>
                <Grid className="clearfix"></Grid>                                                      
            </Grid>
            <Grid className="msg-centr-table-header pad3 ">
                <Grid className="col-md-1"></Grid>
                <Grid className="col-md-2">Recieved</Grid>
                <Grid className="col-md-3">Case Name</Grid>
                <Grid className="col-md-2">ID</Grid>
                <Grid className="col-md-4">Subject</Grid>
                <Grid className="clearfix"></Grid>                            
            </Grid>
            <Grid className="pad3"></Grid>
            <Grid className="msg-centr-odd-row pad3 ">
                <Grid className="col-md-1">&#x2709;</Grid>
                <Grid className="col-md-2">08/07/1017</Grid>
                <Grid className="col-md-3">Ivanov Ivanoc</Grid>
                <Grid className="col-md-2">7639</Grid>
                <Grid className="col-md-4">prescribed app state change</Grid>
                <Grid className="clearfix"></Grid>                            
            </Grid>
            <Grid className="msg-centr-even-row pad3 ">
                <Grid className="col-md-1">&#x2709;</Grid>
                <Grid className="col-md-2">08/07/1017</Grid>
                <Grid className="col-md-3">Ivanov Ivanoc</Grid>
                <Grid className="col-md-2">7648</Grid>
                <Grid className="col-md-4 text-danger">prescribed app state change</Grid>
                <Grid className="clearfix"></Grid>                            
            </Grid>
            <Grid className="msg-centr-odd-row pad3 ">
                <Grid className="col-md-1">&#x2709;</Grid>
                <Grid className="col-md-2">08/07/1017</Grid>
                <Grid className="col-md-3">Ivanov Ivanoc</Grid>
                <Grid className="col-md-2 text-danger">7639</Grid>
                <Grid className="col-md-4 text-danger">prescribed app state change</Grid>
                <Grid className="clearfix"></Grid>                            
            </Grid>
            <Grid className="msg-centr-even-row pad3 ">
                <Grid className="col-md-1">&#x2709;</Grid>
                <Grid className="col-md-2">08/07/1017</Grid>
                <Grid className="col-md-3">Ivanov Ivanoc</Grid>
                <Grid className="col-md-2">7648</Grid>
                <Grid className="col-md-4 text-danger">prescribed app state change</Grid>
                <Grid className="clearfix"></Grid>                            
            </Grid>
            <Grid className="msg-centr-odd-row pad3 ">
                <Grid className="col-md-1">&#x2709;</Grid>
                <Grid className="col-md-2">08/07/1017</Grid>
                <Grid className="col-md-3">Ivanov Ivanoc</Grid>
                <Grid className="col-md-2 text-danger">7639</Grid>
                <Grid className="col-md-4 text-danger">prescribed app state change</Grid>
                <Grid className="clearfix"></Grid>                            
            </Grid>
            <Grid className="msg-centr-even-row pad3 ">
                <Grid className="col-md-1">&#x2709;</Grid>
                <Grid className="col-md-2">08/07/1017</Grid>
                <Grid className="col-md-3">Ivanov Ivanoc</Grid>
                <Grid className="col-md-2">7648</Grid>
                <Grid className="col-md-4 text-danger">prescribed app state change</Grid>
                <Grid className="clearfix"></Grid>                            
            </Grid>
        </Grid>
        
    </Grid>
    );
    
  }
};

export default MessageCenter;





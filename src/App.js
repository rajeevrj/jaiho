import React from 'react';
import './style.scss';

class App extends React.Component {
   render() {
      return (
         <div>
            {this.props.children}
         </div>
      );
   }
}

export default App;
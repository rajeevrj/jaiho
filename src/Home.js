import React from 'react';
import Header from './page-components/header/Header';
import SGCaseFormContainer from './page-components/sgcaseform/SGCaseFormContainer';
import TextInput from './ui-components/TextInput';
import GridRow from './ui-components/GridRow';
import GridCol from './ui-components/GridCol';
//import SelectInput from './ui-components/SelectInput';


class Home extends React.Component {
   
   render() {
      return (
      	<div> 
          <Header /> 
          <SGCaseFormContainer />
        </div> 
      );
   }
}


export default Home;
var path = require('path');
//var webpack = require('webpack');

var config = {
   entry: './main.js',
	
   output: {
      path: path.join(__dirname, './dist'),
      publicPath: 'dist',
      filename: 'bundle.js',
   },
	
   devServer: {
      inline: true,
      port: 8081
   },
	
   module: {
      loaders: [
         {
            test: /\.js?$/,
            exclude: /node_modules/,
            loader: 'babel-loader',
				
            query: {
               presets: ['es2015', 'react']
            }
         },{
            test: /\.css$/, loader: 'style-loader!css-loader'
         },
         {
             test: /\.scss/,
             loader: 'style-loader!css-loader!sass-loader'
         },{
            test: /\.(woff2?|ttf|svg|eot)(\?v=\d+\.\d+\.\d+)?$/,
            loader: 'file-loader',
         },{
            test: /\.(jpg|png)$/,
            loader: 'file?name=/[path][name].[hash].[ext]'
         }
      ]
   }
}

module.exports = config;